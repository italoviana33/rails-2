# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Librarian.create!(
    name: "Admin",
    email:"admin@admin",
    password:"123456",
    password_confirmation:"123456"
)
puts ("criando autores e clientes")
10.times do |i|
    Author.create!(
        name: Faker::Name.name
    )
    Client.create!(
        name: Faker::Name.name
    )
end
puts("CLIENTE E AUTORES CRIADOS")
puts("CRIANDO LIVROS")
50.times do |i|
    Book.create!(
        name: Faker::Book.title,
        author: Author.all.sample
    )
end
puts("LIVROS CRIADOS")
puts("criando reservas")
10.times do |i|
    Reservation.create!(
        book:Book.all.sample,
        client: Client.all.sample,
        librarian: Librarian.all.sample,
        active: true
    )
    Reservation.create!(
        book:Book.all.sample,
        client: Client.all.sample,
        librarian: Librarian.all.sample,
        active: false
    )
end
puts("RESERVAS CRIADAS")