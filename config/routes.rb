Rails.application.routes.draw do
  
  
  get 'home/index'
  root to: 'home#index'
  resources :reservations
  resources :librarians
  resources :librarian
  devise_for :librarian, path:'routeto'
  resources :books
  resources :clients
  resources :authors
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
